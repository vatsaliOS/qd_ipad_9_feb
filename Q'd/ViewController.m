//
//  ViewController.m
//  Q'd
//
//  Created by SOTSYS028 on 11/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "ViewController.h"
#import "SignUpVC.h"
#import "DrawerVC.h"
#import "MainVC/MainVC.h"
#import "MBProgressHUD.h"

@interface ViewController ()
{
    IBOutlet UITextField *emailTxtField;
    IBOutlet UITextField *pwdTxtField;
}
@property (weak, nonatomic) IBOutlet UIView *emailView;
@property (weak, nonatomic) IBOutlet UIView *pwdView;
@property (weak, nonatomic) IBOutlet UIButton *signInBtn;
@property (weak, nonatomic) IBOutlet UIButton *createAccount;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard1)];
    [self.view addGestureRecognizer:tap];

    [self changeUI];
}

-(void)viewWillAppear:(BOOL)animated
{
    pwdTxtField.text = @"";
    emailTxtField.text = @"";
//    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)dismissKeyboard1 {
    [emailTxtField resignFirstResponder];
    [pwdTxtField resignFirstResponder];
}


#pragma mark - UI Design using Code

-(void)changeUI
{
    // e,ail View layer
    [self.emailView.layer setCornerRadius:5.0f];
    [self.emailView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.emailView.layer setBorderWidth:1.0f];
    
    // Pwd View layer
    [self.pwdView.layer setCornerRadius:5.0f];
    [self.pwdView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.pwdView.layer setBorderWidth:1.0f];
    
    // SignIn button
    [self.signInBtn.layer setCornerRadius:5.0f];
    [self.signInBtn.layer setBorderColor:[UIColor clearColor].CGColor];
    [self.signInBtn.layer setBorderWidth:2.0f];
    
    // Create Account
    [self.createAccount.layer setCornerRadius:5.0f];
    [self.createAccount.layer setBorderColor:[UIColor clearColor].CGColor];
    [self.createAccount.layer setBorderWidth:2.0f];
    
}

#pragma MARK - IBAction Methods

- (IBAction)signInBtnClk:(id)sender {
    
    SharedObj.isFromLogin = YES;
    
    [emailTxtField resignFirstResponder];
    [pwdTxtField resignFirstResponder];
    
    if ([emailTxtField.text isEqualToString:@""]) {
        DisplayAlertControllerWithTitle(@"Please enter email address", @"Q'd");
        
        return;
    }
    else if ([pwdTxtField.text isEqualToString:@""])
    {
        DisplayAlertControllerWithTitle(@"Please enter password", @"Q'd");
        return;
    }
    
    BOOL isValidEmail = [Common validateEmail:emailTxtField.text];
    
    if (!isValidEmail){
        DisplayAlertControllerWithTitle(@"Please enter valid email address", @"Q'd");
    }
    else
    {
        MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading...";
        
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:emailTxtField.text forKey:@"email"];
        [Dict setValue:pwdTxtField.text forKey:@"password"];
        [Dict setValue:XGET_VALUE(@"DeviceToken") forKey:@"device_token"];
        
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"owner"];
        
        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/auth/signin_owner",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (success)
            {
                NSLog(@"response in success = %@",response);
                //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
                
                NSMutableDictionary *responseDict = [response valueForKey:@"owner"];
                
                [[NSUserDefaults standardUserDefaults]setObject:responseDict forKey:@"OwnerDetails"];
                
                [[NSUserDefaults standardUserDefaults] setValue:[[[responseDict valueForKey:@"restaurants"] objectAtIndex:0] valueForKey:@"average_waiting_time"] forKey:@"waitTime"];

                [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"HasLogin"];
                
                [self performSegueWithIdentifier:@"MainVC" sender:nil];
                
            }
            else
            {
                DisplayAlertControllerWithTitle(@"wrong username or password", @"Q'd");
            }
        }];
        
    }

    
}

- (IBAction)createAccountBtnClk:(id)sender {
    
    [self performSegueWithIdentifier:@"SignUpVC" sender:nil];
}

# pragma mark UITextField Methods

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"SignUpVC"]) {
        SignUpVC *VC = (SignUpVC*)[segue destinationViewController];
    }
    if ([segue.identifier isEqualToString:@"MainVC"]) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLogin"];
        MainVC *VC = (MainVC*)[segue destinationViewController];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
