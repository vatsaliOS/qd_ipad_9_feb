//
//  SignUpVC.m
//  Q'd
//
//  Created by SOTSYS028 on 11/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "SignUpVC.h"

@interface SignUpVC ()
{
    IBOutlet UIView *emailView;
    IBOutlet UIView *phnNumberView;
    IBOutlet UIView *pwdView;
    IBOutlet UIView *confirmPwdView;
    
}
@end

@implementation SignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // e,ail View layer
    [emailView.layer setCornerRadius:5.0f];
    [emailView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [emailView.layer setBorderWidth:1.0f];
    
    // Pwd View layer
    [pwdView.layer setCornerRadius:5.0f];
    [pwdView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [pwdView.layer setBorderWidth:1.0f];
    
    // phnNumber View layer
    [phnNumberView.layer setCornerRadius:5.0f];
    [phnNumberView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [phnNumberView.layer setBorderWidth:1.0f];
    
    // confirmPwd View layer
    [confirmPwdView.layer setCornerRadius:5.0f];
    [confirmPwdView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [confirmPwdView.layer setBorderWidth:1.0f];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

-(IBAction)backBtnClk:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
