
#import "Common.h"
#import <AFNetworking.h>

@implementation Common

//Post Method
+(void)postServiceWithURL:(NSString*)strUrl withParam:(NSDictionary*)dictParam withCompletion:(void(^)(NSDictionary*response,BOOL success1))completion
{
//    if(![self isNetworkReachable]){
//        completion (nil, NO);
//        //AlertViewNetwork
//        return;
//    }
    
//    strUrl = @"http://alpha.besure.com:81/api/user/";
//    dictParam = @{@"TempEmail",@"TempPwd"};
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
        
    [manager POST:strUrl parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject) {        
        NSString *Code = [NSString stringWithFormat:@"%@",[responseObject valueForKey:@"code"]];
        
        if ([Code isEqualToString:@"200"]) {
            completion (responseObject,YES);
        } else {
            completion (responseObject, NO);
        }
        
        NSLog(@"response Object == %@",responseObject);
        
    }
    failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              completion (operation.responseObject, NO);
           //   kCustomAlertWithParamAndTarget(APP_NAME, @"Connection Error !", nil);
              NSLog(@"Inside Failure");
    }];
}

//Get Method
+(void)getServiceWithURL:(NSString *)strUrl withParam:(NSDictionary *)dictParam withCompletion:(void (^)(NSDictionary *, BOOL))completion
{
//    if(![self isNetworkReachable]){
//        completion (nil, NO);
//      //  AlertViewNetwork
//        return;
//    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.responseSerializer.acceptableContentTypes = [manager.responseSerializer.acceptableContentTypes setByAddingObject:@"text/html"];
    
        [manager GET:strUrl parameters:dictParam success:^(AFHTTPRequestOperation *operation, id responseObject)
         {
             if ([[responseObject valueForKey:@"success"] isEqualToString:@"true"]) {
                 completion (responseObject,YES);
             } else {
                 completion (responseObject, NO);
             }
             
             NSLog(@"response Object == %@",responseObject);

         }
             failure:^(AFHTTPRequestOperation *operation, NSError *error) {
              completion (operation.responseObject, NO);
         //     kCustomAlertWithParamAndTarget(APP_NAME, @"Connection Error !", nil);
              NSLog(@"Inside Failure");
                 
          }];
}

//Email Validation
+ (BOOL)validateEmail:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    if ([emailTest evaluateWithObject:email]) {
        return YES;
    }
    else
    {
        return NO;
    }
}

+(BOOL)validatePassword:(NSString *)pwd {
    if ( [pwd length]<6 || [pwd length]>20 )
    {
        if ([pwd length]<8) {
            
            NSString *strMessage ;
            if ([pwd length] == 0) {
                strMessage = @"Password is a required field";
            }
            else
            {
                strMessage = @"Password must contain 6 or more characters";
            }
            
        //    kCustomAlertWithParamAndTarget(APP_NAME, strMessage, nil);
        }
        else {
         //   kCustomAlertWithParamAndTarget(APP_NAME, @"Password must contain less than 20 characters", nil);
        }
        return NO;
    }
    else
    {
        return YES;
    }
}

+(BOOL)isValidPinCode:(NSString*)pincode {
    NSString *pinRegex = @"^([A-PR-UWYZ]([0-9]{1,2}|([A-HK-Y][0-9]|[A-HK-Y][0-9]([0-9]|[ABEHMNPRV-Y]))|[0-9][A-HJKS-UW])\ [0-9][ABD-HJLNP-UW-Z]{2}|(GIR\ 0AA)|(SAN\ TA1)|(BFPO\ (C\/O\ )?[0-9]{1,4})|((ASCN|BBND|[BFS]IQQ|PCRN|STHL|TDCU|TKCA)\ 1ZZ))$";
    NSPredicate *pinTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pinRegex];
    
    BOOL pinValidates = [pinTest evaluateWithObject:pincode];
    return pinValidates;
}

//+(BOOL)isNetworkReachable {
//    [[NetConnection sharedReachability] setHostName:@"www.google.com"];
//    NetworkStatus remoteHostStatus = [[NetConnection sharedReachability] internetConnectionStatus];
//    
//    if (remoteHostStatus == NotReachable)
//        return NO;
//    else if (remoteHostStatus == ReachableViaCarrierDataNetwork || remoteHostStatus == ReachableViaWiFiNetwork)
//        return YES;
//    return NO;
//}

+(BOOL)validateUrl:(NSString *)strURL {
    NSString *urlRegEx =@"^(https|http|Http|Https?)://";
    urlRegEx = [urlRegEx stringByAppendingString:@"(((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))"];
    urlRegEx = [urlRegEx stringByAppendingString:@"|((([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])))"];
    urlRegEx = [urlRegEx stringByAppendingString:@"(:[1-9][0-9]+)?(/)?([/?].+)?$"];
    
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", urlRegEx];
    return [urlTest evaluateWithObject:strURL];
}

+(NSDictionary *)postRequest:(NSString *)url post:(NSString *)post
{
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError *error;
    
    NSData *jsonData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    NSDictionary *results = jsonData ? [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves error:&error] : nil;
    
    if (error)
    {
        NSLog(@"[%@ %@] JSON error: %@", NSStringFromClass([self class]), NSStringFromSelector(_cmd), error.localizedDescription);
        dispatch_async(dispatch_get_main_queue(),
                       ^{
              //             kCustomAlertWithParamAndTarget(APP_NAME, @"Internal Server Error", nil);
                       });
    }
    return results;
}

+(void)callWebService:(NSString *)post JSONstring:(NSString *)strJSON  completion:(completion) compblock
{
    if([self isNetworkReachable])
    {dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                    ^{
                        NSDictionary *dict = [Common postRequest:post post:strJSON];
                        dispatch_async(dispatch_get_main_queue(),
                                       ^{
                                           if (dict != nil) {
                                               
                                               if ([[dict valueForKey:@"Description"] isEqualToString:@"Successful Login"] || [[dict valueForKey:@"Description"] isEqualToString:@"Success"]) {
                                                   compblock(YES,dict);
                                               }
                                               else
                                               {
                                                   compblock(NO,dict);
                                               }
                                           }
                                           else
                                           {
                                     //          kCustomAlertWithParamAndTarget(APP_NAME, @"Internal Server Error", nil);
                                               compblock(NO,dict);
                                           }
                                           
                                       });

                    });
    }
    else
    {
    //    kCustomAlertWithParamAndTarget(APP_NAME, @"Internet is not available", nil);
        compblock(NO,nil);
    }
}

//+(NSString*)GetTokenWithNonce:(NSString *)nonce Timestamp:(NSString *)timestamp
//{
//    NSString *strurl=[NSString stringWithFormat:@"nonce=%@&timestamp=%@", nonce, timestamp];
//    strurl=[strurl stringByAppendingString:@"|"];
//    NSString *strData=[strurl stringByAppendingString:SECRET_KEY];
//    NSString *result = [Global hashedString:strData withKey:PRIVATE_KEY];
//    return result;
//}
//
//+(void)callGetNotificationcount:(void (^)(int counts))completion2 {
//    __block int notificationCount;
//    NSString *post;
//    post = [NSString stringWithFormat:@"%@%@",kServeMainUrl,@"getNotificationcount"];
//    
//    NSMutableDictionary *dict = [NSMutableDictionary new];
//    
//    NSString *strNonce = [Global EncodeString:[Global randomStringWithLength:6]];
//    NSString *strTimeStamp = [Global EncodeString:[Global GetCurrentTimeStamp]];
//    NSString *strToken = [Common GetTokenWithNonce:strNonce Timestamp:strTimeStamp];
//    
//    [dict setValue:XGET_STRING(@"UserId") forKey:@"iUserId"];
//    [dict setValue:strNonce forKey:@"nonce"];
//    [dict setValue:strTimeStamp forKey:@"timestamp"];
//    [dict setValue:strToken forKey:@"token"];
//    
//    [Common postServiceWithURL:post withParam:dict withCompletion:^(NSDictionary *response, BOOL success) {
//        if(success) {
//            if ([[response valueForKey:@"code"]boolValue]) {
//                notificationCount = [[[[response valueForKey:@"notifications"] objectAtIndex:0] valueForKey:@"Totalnotification"] intValue];
//                completion2(notificationCount);
//            }
//            else {
//                //if Fail pass 0
//                completion2(0);
//            }
//        }
//        else {
//            //if Fail pass 0
//            completion2(0);
//        }
//    }];
//}
//
//+(void)updateBadgeCount {
//    NSString *post;
//    post = [NSString stringWithFormat:@"%@%@",kServeMainUrl,@"updateBadgeCount"];
//    
//    NSMutableDictionary *dict = [NSMutableDictionary new];
//    
//    NSString *strNonce = [Global EncodeString:[Global randomStringWithLength:6]];
//    NSString *strTimeStamp = [Global EncodeString:[Global GetCurrentTimeStamp]];
//    NSString *strToken = [Common GetTokenWithNonce:strNonce Timestamp:strTimeStamp];
//    
//    [dict setValue:XGET_STRING(@"UserId") forKey:@"iUserId"];
//    [dict setValue:strNonce forKey:@"nonce"];
//    [dict setValue:strTimeStamp forKey:@"timestamp"];
//    [dict setValue:strToken forKey:@"token"];
//    
//    [Common postServiceWithURL:post withParam:dict withCompletion:^(NSDictionary *response, BOOL success) {
//        if(success) {
//            if ([[response valueForKey:@"code"]boolValue]) {
//                [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
//            }
//            else {
//                NSLog(@"Error in updateBadgeCount");
//            }
//        }
//        else {
//            NSLog(@"Error in updateBadgeCount");
//        }
//    }];
//
//}

@end
