//
//  MainVCCell.h
//  Qd
//
//  Created by SOTSYS028 on 16/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell.h>

@interface MainVCCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *clockLbl;
@property (weak, nonatomic) IBOutlet UILabel *phnNoLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalMinLbl;
@property (weak, nonatomic) IBOutlet UILabel *totalPeopleLbl;
@property (weak, nonatomic) IBOutlet UIImageView *backImgView;
@property (weak, nonatomic) IBOutlet UILabel *minLbl;
@property (weak, nonatomic) IBOutlet UILabel *peopleLbl;
@property (weak, nonatomic) IBOutlet UIView *sideView;
@property (weak,nonatomic)  IBOutlet UIButton *starImgBtn;

@end
