//
//  MainVC.m
//  Qd
//
//  Created by SOTSYS028 on 16/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "MainVC.h"
#import "MainVCCell.h"
#import <SWTableViewCell.h>
#import "MBProgressHUD.h"

#import "ViewController.h"
#import <IQKeyboardManager.h>

#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
#define ACCEPTABLE_NUMBERS @"0123456789"
#define ACCEPTABLE_Phone_NUMBERS @"0123456789"


@interface MainVC ()
{
    IBOutlet UITableView *tblView;
    NSIndexPath *previousIndexPath;

    __weak IBOutlet UIView *removePartySeatPartyView;
    // Below is us
    __weak IBOutlet UILabel *deleteViewLbl;
    __weak IBOutlet UIButton *deleteViewCancelBtn;
    __weak IBOutlet UIButton *deleteViewRemoveBtn;
    
    __weak IBOutlet UIButton *slowModeBtn;
    __weak IBOutlet UIView *addWalkInView;
    __weak IBOutlet UIView *addWalkinCircle;
    
    // Change Wait Time View
    __weak IBOutlet UIView *changeWaitTimeView;
    
    IBOutlet UIImageView *restaurantImgView;
    IBOutlet UILabel *waitingTimeLbl;
    IBOutlet UILabel *restaurantNameLbl;
    IBOutlet UILabel *addressLbl;
    
    BOOL isSlowModeClick;
    NSMutableArray *restaurantArray;
    
    NSMutableArray *userListArray;
    NSInteger deleteRowIndex;
    NSInteger seatThisPartIndex;
    
    // Add walkIn View
    
    __weak IBOutlet UITextField *nameTxtField;
    __weak IBOutlet UITextField *countryCodeTxtField;
    __weak IBOutlet UITextField *phnNoTxtField;
    __weak IBOutlet UITextField *noOfPeopleTxtField;
    int pageOffset;
    
    int totalCount;
    
     BOOL isPageRefresing;
    BOOL isMorePage;

    __weak IBOutlet NSLayoutConstraint *widthConstraint;
    __weak IBOutlet UIActivityIndicatorView *activityIndicator;
    
    BOOL isChangeWaitTime;
    
    UIRefreshControl *refreshControl;
    CGFloat yOffset;
    NSMutableArray *countryCodeArray;
    __weak IBOutlet UITableView *countryCodeTblView;
    
    // added on 14-3
    BOOL isRefreshCalled;
    
    NSTimer *countTimer;

}

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    countryCodeTblView.hidden = YES;
    
    yOffset = 0.0;
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshCalled) forControlEvents:UIControlEventValueChanged];
    [tblView addSubview:refreshControl];
    
    [[IQKeyboardManager sharedManager] setEnable:false];
    
    activityIndicator.alpha = 0.0;
    
    // Load Country Data
    NSError *err;
    NSString *path=[[NSBundle mainBundle]pathForResource:@"Country" ofType:@"json"];
    NSURL *url=[NSURL fileURLWithPath:path];
    NSURLRequest *req=[NSURLRequest requestWithURL:url];
    NSData *data = [NSURLConnection sendSynchronousRequest:req returningResponse:nil error:&err];
    NSDictionary *json=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    countryCodeArray=[[NSMutableArray alloc]init];
    countryCodeArray=[json valueForKey:@"countries"];
    
    // till here , country data
    
//    self.blurViewYConstraint.constant = self.view.frame.size.height;
//    self.deleteViewCenterYconstraint.constant = self.view.frame.size.height/2;    
    self.blurViewYConstraint.constant = 0;
    self.deleteViewCenterYconstraint.constant = 0;
    
    [self.blurView setAlpha:0];
    [removePartySeatPartyView setAlpha:0];
    
//    pageOffset = 1;
    
//    self.blurView.hidden = YES;
    previousIndexPath = nil;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CellSwipped:) name:@"CellSwipped" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(CellSwippedEnd:) name:@"CellSwippedEnd" object:nil];
    
    NSLog(@"Detail == %@",[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"OwnerDetails"]);
    restaurantArray = [[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"OwnerDetails"] objectForKey:@"restaurants"];
    
    [[NSUserDefaults standardUserDefaults] setValue:[[restaurantArray objectAtIndex:0] valueForKey:@"average_waiting_time"] forKey:@"waitTime"];
    
    userListArray = [[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(updateWaitTimeLabel) name:@"updateWaitTimeLabel" object:nil];

    [self joinListAPI];
    
    [self updateWaitTimeLabel];

}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    [self loadUI];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}

-(void)loadUI
{
    [restaurantImgView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[[restaurantArray objectAtIndex:0] valueForKey:@"image"]]]];
    restaurantNameLbl.text = [NSString stringWithFormat:@"%@",[[restaurantArray objectAtIndex:0]valueForKey:@"name"]];
//    waitingTimeLbl.text = [NSString stringWithFormat:@"%@ minutes",[[restaurantArray objectAtIndex:0]valueForKey:@"average_waiting_time"]];
    waitingTimeLbl.text = [NSString stringWithFormat:@"%@ minutes",[[NSUserDefaults standardUserDefaults]valueForKey:@"waitTime"]];
    
    addressLbl.text = [NSString stringWithFormat:@"%@",[[restaurantArray objectAtIndex:0]valueForKey:@"address"]];

    if ([[[restaurantArray objectAtIndex:0] valueForKey:@"slowmode"] isEqualToNumber:[NSNumber numberWithInt:1]]) {
        
        widthConstraint.constant = 120.0f;
        [slowModeBtn setBackgroundImage:[UIImage imageNamed:@"SlowModeON"] forState:UIControlStateNormal];
        [slowModeBtn setSelected:YES];
    }
    else
    {
        widthConstraint.constant = 100.0f;
        [slowModeBtn setBackgroundImage:[UIImage imageNamed:@"SlowMode"] forState:UIControlStateNormal];
        [slowModeBtn setSelected:NO];
    }

}

/*
-(void)viewDidAppear:(BOOL)animated
{
    isChangeWaitTime = YES;
    
    self.dialView = [[CKCircleView alloc] initWithFrame:CGRectMake(24, 0, addWalkinCircle.frame.size.width-5, addWalkinCircle.frame.size.height-5)];
    
    self.dialView.arcColor = [UIColor colorWithRed:228/255.0 green:15/255.0 blue:62/255.0 alpha:1.0];
    self.dialView.backColor = [UIColor clearColor];
    self.dialView.dialColor = [UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0];
    self.dialView.arcRadius = 80;
    //    self.dialView.units = @"hours";
//    self.dialView.minNum = 0;   // Change this to show current waiting time
    
    self.dialView.minNum = [[[NSUserDefaults standardUserDefaults]valueForKey:@"waitTime"] intValue];
    
    self.dialView.maxNum = 150;
    
    self.dialView.labelColor = [UIColor blackColor];
    self.dialView.labelFont = [UIFont fontWithName:@"Hind-Regular" size:45];
    
    // Added by vatsal on 26-Feb
    
    self.dialView.numberLabel.text = waitingTimeLbl.text;

    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 2, 152, 153)];
    
    // till here
    
    [imgView setImage:[UIImage imageNamed:@"CircleBG"]];
    //  [self.dialView addSubview:imgView];
    
    [addWalkinCircle insertSubview:imgView belowSubview:self.dialView];
    // till here
    
    [addWalkinCircle addSubview: self.dialView];
}

*/

-(void)refreshCalled
{
    if (totalCount >= userListArray.count) {
        isRefreshCalled = true;
        tblView.userInteractionEnabled = NO;
//        pageOffset = 1;
        [userListArray removeAllObjects];
        userListArray = [[NSMutableArray alloc]init];
        [self joinListAPI];
    }
}

// added on 15-3
#pragma mark - Update UI Every Mintue
-(void)updateWaitTimeLabel {
   [countTimer invalidate];
   countTimer = [NSTimer scheduledTimerWithTimeInterval:60 target:self selector:@selector(updateEveryMintue) userInfo:nil repeats:YES];
}

#pragma mark - EveryMinute Update
-(void)updateEveryMintue
{
    [userListArray removeAllObjects];
    userListArray = [[NSMutableArray alloc]init];
//    pageOffset = 1;
    
    if (SharedObj.isNetworkReachable) {
        
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"OwnerDetails"] valueForKey:@"owner_id"]]forKey:@"owner_id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[restaurantArray objectAtIndex:0] valueForKey:@"id"]] forKey:@"restaurant_id"];
        
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"joinqueue"];
//        [mainDict setValue:[NSString stringWithFormat:@"%d",pageOffset] forKey:@"page"];
        
        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/joine_list/",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
            
            if (success)
            {
                NSLog(@"response in success = %@",response);
                //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
                //  [self performSegueWithIdentifier:@"DrawerVC" sender:nil];
                //  DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
                
                userListArray = [[response valueForKey:@"joines"] mutableCopy];
                
//                [userListArray addObjectsFromArray:[response valueForKey:@"joines"]];
                
                totalCount = [[response valueForKey:@"joine_count"] intValue];
                
                waitingTimeLbl.text = [NSString stringWithFormat:@"%@ minutes",[response valueForKey:@"average_waiting_time"]];

                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[response valueForKey:@"average_waiting_time"]] forKey:@"waitTime"];
                
                //            if ([[response valueForKey:@"joine_count"] intValue]%5 >= 0) {
                //                isMorePage=YES;
                //            }
                //            else
                //            {
                //                isMorePage=NO;
                //            }
                
                [tblView reloadData];
                
                isPageRefresing=NO;
                
                if (totalCount == 0) {
//                    DisplayAlertControllerWithTitle(@"No users in Queue", @"Q'd");
                }
                
            }
            else
            {
                NSLog(@"Failure response == %@",response);
                DisplayAlertControllerWithTitle(failureMessage, @"Q'd");
            }
        }];
    }
    else
    {
        DisplayAlertControllerWithTitle(noInternet, @"Q'd");
    }

}
// till here on 15-3

#pragma mark - GetJoinedUsersList API
-(void)joinListAPI
{
    
    if (SharedObj.isNetworkReachable) {
        
        if (!isRefreshCalled) {
            MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.labelText = @"Loading...";
        }
        
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"OwnerDetails"] valueForKey:@"owner_id"]]forKey:@"owner_id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[restaurantArray objectAtIndex:0] valueForKey:@"id"]] forKey:@"restaurant_id"];
        
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"joinqueue"];
//        [mainDict setValue:[NSString stringWithFormat:@"%d",pageOffset] forKey:@"page"];
        

        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/joine_list/",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
            
            if (isRefreshCalled) {
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    isRefreshCalled = false;
                });
                
                [refreshControl endRefreshing];
            }
            else
            {
                [MBProgressHUD hideHUDForView:self.view animated:YES];
            }
            
            if (success)
            {
                NSLog(@"response in success = %@",response);
                //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
                //  [self performSegueWithIdentifier:@"DrawerVC" sender:nil];
                //  DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
                
                userListArray = [[response valueForKey:@"joines"] mutableCopy];
                
//                [userListArray addObjectsFromArray:[response valueForKey:@"joines"]];
                
                totalCount = [[response valueForKey:@"joine_count"] intValue];
                
                waitingTimeLbl.text = [NSString stringWithFormat:@"%@ minutes",[response valueForKey:@"average_waiting_time"]];
                
                [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",[response valueForKey:@"average_waiting_time"]] forKey:@"waitTime"];
                
                //            if ([[response valueForKey:@"joine_count"] intValue]%5 >= 0) {
                //                isMorePage=YES;
                //            }
                //            else
                //            {
                //                isMorePage=NO;
                //            }
                
                [tblView reloadData];
//                [refreshControl endRefreshing];

                isPageRefresing=NO;
                
                tblView.userInteractionEnabled = YES;
                
                if (totalCount == 0) {
                    DisplayAlertControllerWithTitle(@"No users in Queue", @"Q'd");
                }
                
                
                
            }
            else
            {
                tblView.userInteractionEnabled = YES;
                DisplayAlertControllerWithTitle(failureMessage, @"Q'd");
                
                isRefreshCalled = false;
            }
        }];
    }
    else
    {
        DisplayAlertControllerWithTitle(noInternet, @"Q'd");
    }
    
}

#pragma mark - NotificationCall for Cell
- (void) CellSwipped:(NSNotification *) notification
{
//    NSLog(@"Notification = %@",notification);
    
    NSIndexPath *path = [notification object];
    MainVCCell *cell = (MainVCCell*)[tblView cellForRowAtIndexPath:path];
    [cell.sideView setBackgroundColor:[UIColor whiteColor]];
    deleteRowIndex = path.row;

//    [cell.sideView setBackgroundColor:[UIColor whiteColor]];
}

- (void) CellSwippedEnd:(NSNotification *) notification
{
//    NSLog(@"Notification = %@",notification);
    
    NSIndexPath *path = [notification object];
    MainVCCell *cell = (MainVCCell*)[tblView cellForRowAtIndexPath:path];
    [cell.sideView setBackgroundColor:[UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0]];
    
    //    [cell.sideView setBackgroundColor:[UIColor whiteColor]];
}


#pragma mark - Tableview Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == countryCodeTblView) {
        return countryCodeArray.count;
    }
    return userListArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (tableView == countryCodeTblView) {
        
        UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"countryCell"];
        if (cell == nil) {
            cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"countryCell"];
        }
        cell.textLabel.text = [NSString stringWithFormat:@"%@ : %@",[[countryCodeArray objectAtIndex:indexPath.row]valueForKey:@"name"],[[countryCodeArray objectAtIndex:indexPath.row]valueForKey:@"code"]];
        cell.textLabel.font = [UIFont fontWithName:@"HindVadodara-Regular" size:20.0f];
        
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        return cell;
    }
    else{
        
        MainVCCell *cell = (MainVCCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
        
        if (cell == nil) {
            cell = (MainVCCell*)[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        
        if (userListArray.count > 0) {
            cell.starImgBtn.tag = indexPath.row;
//            [cell.starImgBtn addTarget:self action:@selector(starBtnClk:) forControlEvents:UIControlEventTouchUpInside];
            
//            if ([[[userListArray objectAtIndex:indexPath.row]valueForKey:@"is_walkin_customer"] boolValue]) {
//                [cell.starImgBtn setHidden:YES];
//            }
//            else
//            {
                if ([[[userListArray objectAtIndex:indexPath.row]valueForKey:@"star"]boolValue]) {
                    [cell.starImgBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"PinkStar"]] forState:UIControlStateNormal];
                    cell.totalMinLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
                    cell.totalPeopleLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
                    cell.nameLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
                    [cell.starImgBtn setSelected:YES];
                }
                else{
                    [cell.starImgBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"GrayStar"]] forState:UIControlStateNormal];
                    cell.totalMinLbl.textColor = [UIColor blackColor];
                    cell.totalPeopleLbl.textColor = [UIColor blackColor];
                    cell.nameLbl.textColor = [UIColor blackColor];
                    [cell.starImgBtn setSelected:NO];
                }
//            }
            
            cell.nameLbl.text = [NSString stringWithFormat:@"%@",[[userListArray objectAtIndex:indexPath.row]valueForKey:@"fullname"]];
            cell.phnNoLbl.text = [NSString stringWithFormat:@"%@",[[userListArray objectAtIndex:indexPath.row]valueForKey:@"phone"]];
            cell.totalMinLbl.text = [NSString stringWithFormat:@"%d",[[[userListArray objectAtIndex:indexPath.row]valueForKey:@"estimate_wait_time"] intValue]];
            cell.totalPeopleLbl.text = [NSString stringWithFormat:@"%@",[[userListArray objectAtIndex:indexPath.row]valueForKey:@"number_of_people"]];
            
            [cell.sideView setBackgroundColor:[UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0]];
            
            NSMutableArray *rightUtilityButtons = [NSMutableArray new];
            //    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor darkGrayColor] icon:[UIImage imageNamed:@"EditTableImage"]];
            [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor redColor] icon:[UIImage imageNamed:@"Removebtn"]];
            cell.rightUtilityButtons = rightUtilityButtons;
            cell.delegate = self;
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        return cell;

    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == countryCodeTblView) {
        countryCodeTxtField.text = [[countryCodeArray objectAtIndex:indexPath.row]valueForKey:@"code"];
        countryCodeTblView.hidden = YES;
        [phnNoTxtField becomeFirstResponder];
        isPageRefresing = NO;
    }
    else
    {
        MainVCCell *cell = (MainVCCell *)[tableView cellForRowAtIndexPath:indexPath];
        cell.nameLbl.textColor = [UIColor whiteColor];
        cell.phnNoLbl.textColor = [UIColor whiteColor];
        cell.totalMinLbl.textColor = [UIColor whiteColor];
        cell.totalPeopleLbl.textColor = [UIColor whiteColor];
        cell.minLbl.textColor = [UIColor whiteColor];
        cell.peopleLbl.textColor = [UIColor whiteColor];
        cell.backImgView.backgroundColor = [UIColor colorWithRed:228/255.0 green:15/255.0 blue:61/255.0 alpha:1.0];
        
        //    [cell.sideView setBackgroundColor:[UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0]];
        
        if (previousIndexPath != nil) {
            MainVCCell *cell = (MainVCCell *)[tableView cellForRowAtIndexPath:previousIndexPath];
            
            //        if (previousIndexPath.row == 3 || previousIndexPath.row == 4 || previousIndexPath.row == 7) {
            if ([[[userListArray objectAtIndex:indexPath.row]valueForKey:@"star"]boolValue]) {
                cell.totalMinLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
                cell.totalPeopleLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
                cell.nameLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
            }
            else
            {
                cell.nameLbl.textColor = [UIColor blackColor];
                cell.totalMinLbl.textColor = [UIColor blackColor];
                cell.totalPeopleLbl.textColor = [UIColor blackColor];
            }
            
//            cell.backImgView.backgroundColor = [UIColor clearColor];  // 21-3
            cell.phnNoLbl.textColor = [UIColor blackColor];
            cell.minLbl.textColor = [UIColor lightGrayColor];
            cell.peopleLbl.textColor = [UIColor lightGrayColor];
            
        }
        
        //    if (previousIndexPath.row == 3 || previousIndexPath.row == 4 || previousIndexPath.row == 7) {
        if ([[[userListArray objectAtIndex:indexPath.row]valueForKey:@"star"]boolValue]){
            
            cell.totalMinLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
            cell.totalPeopleLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
            cell.nameLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
        }
        previousIndexPath = indexPath;
        
        // Added on 20-2
        
        seatThisPartIndex = indexPath.row;
        [addWalkInView setHidden:YES];
        [changeWaitTimeView setHidden:YES];
        
        [removePartySeatPartyView setHidden:NO];
        
        isSlowModeClick = YES;      // When Seat This pArty View will come as popup , then "Yes" and During Delete Row , nake it false
        
        deleteViewLbl.text = @"Are you sure you are ready to seat this party?";
        [deleteViewRemoveBtn setTitle:@"CONFIRM" forState:UIControlStateNormal];
        
        [self deleteAndSlowDownAnimation];

    }
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView != countryCodeTblView) {
        [self deselectRow];
    }
}

// Commented on 14-3
/*
#pragma mark - StarImgBtnClick

-(void)starBtnClk : (UIButton*)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    
//    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.labelText = @"Loading...";
    
    if (SharedObj.isNetworkReachable) {

    
    NSMutableDictionary *Dict = [NSMutableDictionary new];
    [Dict setValue:[NSString stringWithFormat:@"%@",[[userListArray objectAtIndex:indexPath.row]valueForKey:@"user_id"]] forKey:@"user_id"];
    [Dict setValue:[NSString stringWithFormat:@"%@",[[restaurantArray objectAtIndex:0] valueForKey:@"id"]] forKey:@"restaurant_id"];
    
    NSMutableDictionary *mainDict = [NSMutableDictionary new];
    [mainDict setObject:Dict forKey:@"star"];
    
    [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/mark_loyal",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
//        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if (success)
        {
            NSLog(@"response in success = %@",response);
            //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
            //  [self performSegueWithIdentifier:@"DrawerVC" sender:nil];
            //  DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
            
//            [tblView reloadData];
            
            MainVCCell *cell = (MainVCCell *)[tblView cellForRowAtIndexPath:indexPath];
            
            if (sender.isSelected){
                [cell.starImgBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"GrayStar"]] forState:UIControlStateNormal];
                cell.totalMinLbl.textColor = [UIColor blackColor];
                cell.totalPeopleLbl.textColor = [UIColor blackColor];
                cell.nameLbl.textColor = [UIColor blackColor];
                [cell.starImgBtn setSelected:NO];
            }
            else
            {
                [cell.starImgBtn setImage:[UIImage imageNamed:[NSString stringWithFormat:@"PinkStar"]] forState:UIControlStateNormal];
                cell.totalMinLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
                cell.totalPeopleLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
                cell.nameLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
                [cell.starImgBtn setSelected:YES];
            }

        }
        else
        {
            DisplayAlertControllerWithTitle(failureMessage, @"Q'd");
        }
    }];
        
    }
    else
    {
        DisplayAlertControllerWithTitle(noInternet, @"Q'd");
    }

}
 */
// till here on 14-3

#pragma mark - Swipeable TableViewCell
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    
    [cell setBackgroundColor:[UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0]];
    
     switch (index) {
        case 0:
        {
            [self showDeleteButtonView:(int)index];
            break;
        }
        default:
            break;
    }
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    return TRUE;
}

-(void)showDeleteButtonView:(int)Index
{
    isSlowModeClick = NO;
    
    [tblView reloadData];
    
    NSIndexPath *path = [NSIndexPath indexPathForRow:Index inSection:0];
    MainVCCell *cell1 = (MainVCCell*)[tblView cellForRowAtIndexPath:path];
    [cell1.sideView setBackgroundColor:[UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0]];

    [addWalkInView setHidden:YES];
    [changeWaitTimeView setHidden:YES];
    
    [removePartySeatPartyView setHidden:NO];
    
    deleteViewLbl.text = @"Are you sure you want remove this party?";
    [deleteViewRemoveBtn setTitle:@"REMOVE" forState:UIControlStateNormal];

    [self deleteAndSlowDownAnimation];
}

#pragma mark Animation for SlowDown and Delete Row Click
-(void)deleteAndSlowDownAnimation
{
    
//    isSlowModeClick = NO;
    
//    [self.blurView setDynamic:NO];
    [self.blurView setBlurRadius:18.0f];
    [UIView animateWithDuration:0.2 animations:^{
        
        [self.blurView setBackgroundColor:[UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:0.8]];
//        self.blurViewYConstraint.constant = 0;
//        self.deleteViewCenterYconstraint.constant = 0;
        
        [self.blurView setAlpha:1.0];
        [removePartySeatPartyView setAlpha:1.0];
        self.blurView.tintColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.0];

        [self.view layoutIfNeeded];
        
    }completion:^(BOOL finished)
     {
         //         [self.blurView setAlpha:1.0f];
//         self.blurView.tintColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.0];
         [[UIApplication sharedApplication]setStatusBarHidden:YES];  // Hide status bar
         
     }];
}


#pragma mark IBAction methods for Delete Row and SlowDown Click
-(IBAction)cancelBtnClk:(id)sender
{
    [self.blurView setAlpha:0];
    [removePartySeatPartyView setAlpha:0.0];
    
    if (isSlowModeClick) {
        [self deselectRow];
    }
    
//    self.blurViewYConstraint.constant = self.view.frame.size.height;
//    self.deleteViewCenterYconstraint.constant = self.view.frame.size.height/2;
}

-(IBAction)removeBtnClk:(id)sender
{
    [self.blurView setAlpha:0];
    [removePartySeatPartyView setAlpha:0.0];

    if (isSlowModeClick) {
//        [self updateSlowTimeAlertAPI];
        [self readyToSeat:seatThisPartIndex];   // Show popUp of Are you sure, you want to seat this party
        [self deselectRow];
    }
    else
    {
        [self deleteAPI:deleteRowIndex];
    }
    
//    self.blurViewYConstraint.constant = self.view.frame.size.height;
//    self.deleteViewCenterYconstraint.constant = self.view.frame.size.height/2;
}

#pragma mark - DeselectRow Method

-(void)deselectRow
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:seatThisPartIndex inSection:0];
    
    MainVCCell *cell = (MainVCCell *)[tblView cellForRowAtIndexPath:indexPath];
    cell.nameLbl.textColor = [UIColor whiteColor];
    cell.phnNoLbl.textColor = [UIColor whiteColor];
    cell.totalMinLbl.textColor = [UIColor whiteColor];
    cell.totalPeopleLbl.textColor = [UIColor whiteColor];
    cell.minLbl.textColor = [UIColor whiteColor];
    cell.peopleLbl.textColor = [UIColor whiteColor];
    cell.backImgView.backgroundColor = [UIColor colorWithRed:228/255.0 green:15/255.0 blue:61/255.0 alpha:1.0];
    
    //    [cell.sideView setBackgroundColor:[UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0]];
    
    //        if (previousIndexPath.row == 3 || previousIndexPath.row == 4 || previousIndexPath.row == 7) {
    if ([[[userListArray objectAtIndex:indexPath.row]valueForKey:@"star"]boolValue]) {
        cell.totalMinLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
        cell.totalPeopleLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
        cell.nameLbl.textColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:78/255.0 alpha:1.0];
    }
    else
    {
        cell.nameLbl.textColor = [UIColor blackColor];
        cell.totalMinLbl.textColor = [UIColor blackColor];
        cell.totalPeopleLbl.textColor = [UIColor blackColor];
    }
    
    cell.backImgView.backgroundColor = [UIColor clearColor];
    cell.phnNoLbl.textColor = [UIColor blackColor];
    cell.minLbl.textColor = [UIColor lightGrayColor];
    cell.peopleLbl.textColor = [UIColor lightGrayColor];

    [tblView reloadData];
}

#pragma mark - DeleteRowAPI

-(void)deleteAPI : (NSInteger)rowToDelete
{
    if (SharedObj.isNetworkReachable) {
        MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading...";
        
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"OwnerDetails"] valueForKey:@"owner_id"]]forKey:@"owner_id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[restaurantArray objectAtIndex:0] valueForKey:@"id"]] forKey:@"restaurant_id"];
        //    [Dict setValue:[NSString stringWithFormat:@"%@",[[userListArray objectAtIndex:rowToDelete]valueForKey:@"user_id"]] forKey:@"user_id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[userListArray objectAtIndex:rowToDelete]valueForKey:@"id"]] forKey:@"id"];
        
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"joinqueue"];
        
        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/remove_party/",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (success)
            {
                NSLog(@"response in success = %@",response);
                //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
                //  [self performSegueWithIdentifier:@"DrawerVC" sender:nil];
                //  DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
                
                [userListArray removeObjectAtIndex:rowToDelete];
                totalCount--;
                [tblView reloadData];
            }
            else
            {
                DisplayAlertControllerWithTitle(failureMessage, @"Q'd");
            }
        }];
    }
    else
    {
        DisplayAlertControllerWithTitle(noInternet, @"Q'd");
    }
   
}

#pragma mark - ReadyToSeat API

-(void)readyToSeat : (NSInteger)index
{
    
    if (SharedObj.isNetworkReachable) {
        
        MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading...";
        
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"OwnerDetails"] valueForKey:@"owner_id"]]forKey:@"owner_id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[restaurantArray objectAtIndex:0] valueForKey:@"id"]] forKey:@"restaurant_id"];
        //    [Dict setValue:[NSString stringWithFormat:@"%@",[[userListArray objectAtIndex:index]valueForKey:@"user_id"]] forKey:@"user_id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[userListArray objectAtIndex:index]valueForKey:@"id"]] forKey:@"id"];
        
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"joinqueue"];
        
        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/ready_to_seat/",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (success)
            {
                NSLog(@"response in success = %@",response);
                //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
                //  [self performSegueWithIdentifier:@"DrawerVC" sender:nil];
                //  DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
                
                
                [tblView reloadData];     // commented on 15-3
                
                [userListArray removeAllObjects];
                userListArray = [[NSMutableArray alloc]init];
//                pageOffset = 1;
                [self joinListAPI];     // Added on 15-3
                
                [self testAPICall];
                
                
            }
            else
            {
                DisplayAlertControllerWithTitle(failureMessage, @"Q'd");
            }
        }];
    }
    else
    {
        DisplayAlertControllerWithTitle(noInternet, @"Q'd");
    }

}

-(void)testAPICall
{
    [Common getServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/after_seated",BASEURL] withParam:nil withCompletion:^(NSDictionary *response, BOOL success1) {
        NSLog(@"Response  == %@",response);
    }];

}

#pragma mark Slow Mode Click
- (IBAction)slowModeBtnClk:(id)sender
{
    [self updateSlowTimeAlertAPI];
}

#pragma mark - UpdateSlowTimeAlertAPI
-(void)updateSlowTimeAlertAPI
{
//    MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    hud.labelText = @"Loading...";
    
    activityIndicator.alpha = 1.0;
    
    if (SharedObj.isNetworkReachable) {
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"OwnerDetails"] valueForKey:@"owner_id"]]forKey:@"owner_id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[restaurantArray objectAtIndex:0] valueForKey:@"id"]] forKey:@"id"];
        
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"restaurant"];
        
        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/update_slowmode_time_on_off/",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
            
            activityIndicator.alpha = 0.0;
            //        [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (success)
            {
                NSLog(@"response in success = %@",response);
                //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
                //  [self performSegueWithIdentifier:@"DrawerVC" sender:nil];
                //  DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
                
                if (slowModeBtn.isSelected) {
                    
                    widthConstraint.constant = 100.0f;
                    [slowModeBtn setBackgroundImage:[UIImage imageNamed:@"SlowMode"] forState:UIControlStateNormal];
                    [slowModeBtn setSelected:NO];
                }
                else
                {
                    widthConstraint.constant = 120.0f;
                    [slowModeBtn setBackgroundImage:[UIImage imageNamed:@"SlowModeON"] forState:UIControlStateNormal];
                    [slowModeBtn setSelected:YES];
                }
                
            }
            else
            {
                DisplayAlertControllerWithTitle(failureMessage, @"Q'd");
            }
        }];
    }
    else
    {
        DisplayAlertControllerWithTitle(noInternet, @"Q'd");
    }

    
    
}


#pragma mark Add WalkIn Click
-(IBAction)addWalkInBtnClk:(id)sender
{
    [removePartySeatPartyView setHidden:YES];
    [changeWaitTimeView setHidden:YES];
    
    [addWalkInView setHidden:NO];
    
    countryCodeTxtField.text = @"+61";
    
//    [self.blurView setDynamic:NO];
    [self.blurView setBlurRadius:18.0f];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        //        [self.blurView setBackgroundColor:[UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:0.8]];
//        self.blurViewYConstraint.constant = 0;
        
        [self.blurView setAlpha:1.0];
        
        self.blurView.tintColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.0];

        [self.view layoutIfNeeded];
        
    }completion:^(BOOL finished)
     {
         //         [self.blurView setAlpha:1.0f];
//         self.blurView.tintColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.0];
         [[UIApplication sharedApplication]setStatusBarHidden:YES];  // Hide status bar
         
     }];
}

-(IBAction)addWalkInCancelBtnClk:(id)sender
{
    [nameTxtField resignFirstResponder];
    [noOfPeopleTxtField resignFirstResponder];
    [phnNoTxtField resignFirstResponder];
    
    nameTxtField.text = @"";
    phnNoTxtField.text = @"";
    noOfPeopleTxtField.text = @"";
    
    [self.blurView setAlpha:0];

//    self.blurViewYConstraint.constant = self.view.frame.size.height;
}

-(IBAction)addWalkInAddPartyBtnClk:(id)sender
{

    if ([nameTxtField.text isEqualToString:@""] || [phnNoTxtField.text isEqualToString:@""] || [noOfPeopleTxtField.text isEqualToString:@""]) {
        DisplayAlertWithTitle(@"All Fields are mandatory", @"Q'd");
    }
    else if (nameTxtField.text.length > 40)
    {
        DisplayAlertControllerWithTitle(@"Please enter valid name", @"Q'd");
    }
    else if (phnNoTxtField.text.length > 15)
    {
        DisplayAlertControllerWithTitle(@"Please enter valid phone number", @"Q'd");
    }
    else if (noOfPeopleTxtField.text.length > 5)
    {
        DisplayAlertControllerWithTitle(@"Please enter valid number of people", @"Q'd");
    }
    else
    {
        [nameTxtField resignFirstResponder];
        [noOfPeopleTxtField resignFirstResponder];
        [phnNoTxtField resignFirstResponder];

        [self.blurView setAlpha:0];
        [self addWalkInAPI];
    }

//    self.blurViewYConstraint.constant = self.view.frame.size.height;

}

#pragma mark AddWalkIn API
-(void)addWalkInAPI
{
    [nameTxtField resignFirstResponder];
    [phnNoTxtField resignFirstResponder];
    [noOfPeopleTxtField resignFirstResponder];
    
    if (SharedObj.isNetworkReachable) {
        MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading...";
        
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"OwnerDetails"] valueForKey:@"owner_id"]]forKey:@"owner_id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[restaurantArray objectAtIndex:0] valueForKey:@"id"]] forKey:@"restaurant_id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",nameTxtField.text] forKey:@"name"];
        [Dict setValue:[NSString stringWithFormat:@"%@%@",countryCodeTxtField.text,phnNoTxtField.text] forKey:@"phone"];
        [Dict setValue:[NSString stringWithFormat:@"%@",noOfPeopleTxtField.text] forKey:@"number_of_people"];
        
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"walkincustomer"];
        
        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/walkincustomers/add_walkin",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (success)
            {
                NSLog(@"response in success = %@",response);
                //  DisplayAlertWithTitle(@"You are successfully Signed Up", @"Successfully signedUp");
                //  [self performSegueWithIdentifier:@"DrawerVC" sender:nil];
                //  DisplayAlertControllerWithTitle([response valueForKey:@"message"], @"Q'd");
                
//                pageOffset = 1;
                
                [userListArray removeAllObjects];
                userListArray = [[NSMutableArray alloc]init];
                [self joinListAPI];
            }
            else
            {
                DisplayAlertControllerWithTitle(failureMessage, @"Q'd");
            }
        }];
    }
    else
    {
        DisplayAlertControllerWithTitle(noInternet, @"Q'd");
    }
    nameTxtField.text = @"";
    phnNoTxtField.text = @"";
    noOfPeopleTxtField.text = @"";

}

#pragma mark Change Wait Time Btn Click
- (IBAction)changeWaitTime:(id)sender
{
    
    [removePartySeatPartyView setHidden:YES];
    [addWalkInView setHidden:YES];
    
    [changeWaitTimeView setHidden:NO];
    
//    [self.blurView setDynamic:NO];
    [self.blurView setBlurRadius:18.0f];
    
    [UIView animateWithDuration:0.5 animations:^{
        
        //        [self.blurView setBackgroundColor:[UIColor colorWithRed:247/255.0 green:247/255.0 blue:247/255.0 alpha:0.8]];
//        self.blurViewYConstraint.constant = 0;
        
        [self.blurView setAlpha:1.0];
        
        self.blurView.tintColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.0];

        [self.view layoutIfNeeded];
        
    }completion:^(BOOL finished)
     {
         //         [self.blurView setAlpha:1.0f];
//         self.blurView.tintColor = [UIColor colorWithRed:255/255.0 green:255/255.0 blue:255/255.0 alpha:0.0];
         [[UIApplication sharedApplication]setStatusBarHidden:YES];  // Hide status bar
         
     }];
    
    //    CGFloat width = addWalkinCircle.frame.size.width;
    
    // Added on 26-Feb
    
//    if (!isChangeWaitTime) {
//        self.dialView = [[CKCircleView alloc] initWithFrame:CGRectMake(24, 0, addWalkinCircle.frame.size.width-5, addWalkinCircle.frame.size.height-5)];
//        
//        self.dialView.arcColor = [UIColor colorWithRed:228/255.0 green:15/255.0 blue:62/255.0 alpha:1.0];
//        self.dialView.backColor = [UIColor clearColor];
//        self.dialView.dialColor = [UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0];
//        self.dialView.arcRadius = 80;
//        //    self.dialView.units = @"hours";
////        self.dialView.minNum = 0;   // Change this to show current waiting time
//        
////        self.dialView.minNum = [[[NSUserDefaults standardUserDefaults]valueForKey:@"waitTime"] intValue];
//        
////        self.dialView.maxNum = 150;
//        
//        self.dialView.labelColor = [UIColor blackColor];
//        self.dialView.labelFont = [UIFont fontWithName:@"Hind-Regular" size:45];
//        
//        self.dialView.numberLabel.text = [NSString stringWithFormat:@"%d",[[[NSUserDefaults standardUserDefaults]valueForKey:@"waitTime"] intValue]];
//        self.dialView.tmpLabel.text = @"minutes";
//        // Added by vatsal on 26-Feb
//        
//        self.dialView.numberLabel.text = waitingTimeLbl.text;
//        
//        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 2, 152, 153)];
//        
//        // till here
//        
//        [imgView setImage:[UIImage imageNamed:@"CircleBG"]];
//        //  [self.dialView addSubview:imgView];
//        
//        [addWalkinCircle insertSubview:imgView belowSubview:self.dialView];
//        // till here
//        
//        [addWalkinCircle addSubview: self.dialView];
//
//    }
//    else
//    {
//        self.dialView.numberLabel.text = [NSString stringWithFormat:@"%d",self.dialView.currentNum];
//        self.dialView.tmpLabel.text = @"minutes";
//    }
    
    // till here
    
    self.dialView = [[CKCircleView alloc] initWithFrame:CGRectMake(24, 0, addWalkinCircle.frame.size.width-5, addWalkinCircle.frame.size.height-5)];
    
    self.dialView.arcColor = [UIColor colorWithRed:228/255.0 green:15/255.0 blue:62/255.0 alpha:1.0];
    self.dialView.backColor = [UIColor clearColor];
    self.dialView.dialColor = [UIColor colorWithRed:229/255.0 green:229/255.0 blue:229/255.0 alpha:1.0];
    self.dialView.arcRadius = 80;
//    //    self.dialView.units = @"hours";
    self.dialView.minNum = 0;   // Change this to show current waiting time
    
//    self.dialView.minNum = [[[NSUserDefaults standardUserDefaults]valueForKey:@"waitTime"] intValue];

//
    
//    
    self.dialView.maxNum = 150;
    self.dialView.labelColor = [UIColor blackColor];
    self.dialView.labelFont = [UIFont fontWithName:@"Hind-Regular" size:45];

    // Added by vatsal on 26-Feb
    
    self.dialView.numberLabel.text = waitingTimeLbl.text;
    
    UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 2, 152, 153)];        // Before
    
    [imgView setImage:[UIImage imageNamed:@"CircleBG"]];
//  [self.dialView addSubview:imgView];
    
    [addWalkinCircle insertSubview:imgView belowSubview:self.dialView];
    // till here
    
    [addWalkinCircle addSubview: self.dialView];
    
    // till here
    //
    
    //
}

- (IBAction)changeWaitTimeAcceptBtnClk:(id)sender {
//    self.blurViewYConstraint.constant = self.view.frame.size.height;
    [self changeWaitingtimeAPI];
    [self.blurView setAlpha:0];
}

- (IBAction)changeWaitTimeCanelBtnClk:(id)sender {
//    self.blurViewYConstraint.constant = self.view.frame.size.height;

    isChangeWaitTime = YES;
    
    [self.blurView setAlpha:0];
    self.dialView.numberLabel.text = @"";
    self.dialView.tmpLabel.text = @"";
    self.dialView.imgView.image = nil;
    [self.dialView removeFromSuperview];
}

-(void)changeWaitingtimeAPI
{
    if (SharedObj.isNetworkReachable) {
        MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading...";
        
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"OwnerDetails"] valueForKey:@"owner_id"]]forKey:@"owner_id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[restaurantArray objectAtIndex:0] valueForKey:@"id"]] forKey:@"id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",self.dialView.numberLabel.text] forKey:@"average_waiting_time"];
        
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"restaurant"];
        
        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/update_average_waiting_time/",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (success)
            {
                NSLog(@"response in success = %@",response);
                
                //            isChangeWaitTime = YES;
                
                NSString *strWaiting = [NSString stringWithFormat:@"%d",self.dialView.currentNum];
                [[NSUserDefaults standardUserDefaults] setValue:strWaiting forKey:@"waitTime"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                //            NSLog(@"Detail == %@",[[NSUserDefaults standardUserDefaults] dictionaryForKey:@"OwnerDetails"]);
                
                //
                //            [[[[[NSUserDefaults standardUserDefaults] objectForKey:@"OwnerDetails"] valueForKey:@"restaurants"] objectAtIndex:0] setValue:self.dialView.numberLabel.text forKey:@"average_waiting_time"];
            }
            else
            {
                DisplayAlertControllerWithTitle(failureMessage, @"Q'd");
            }
        }];
    }
    else
    {
        DisplayAlertControllerWithTitle(noInternet, @"Q'd");
    }
    waitingTimeLbl.text = [NSString stringWithFormat:@"%d minutes",self.dialView.currentNum];
    
    self.dialView.numberLabel.text = @"";
    self.dialView.tmpLabel.text = @"";
    self.dialView.imgView.image = nil;
    [self.dialView removeFromSuperview];
    
}

#pragma mark - ScrollView Delegate

//- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
//    CGPoint offset = aScrollView.contentOffset;
//    CGRect bounds = aScrollView.bounds;
//    CGSize size = aScrollView.contentSize;
//    UIEdgeInsets inset = aScrollView.contentInset;
//    float y = offset.y + bounds.size.height - inset.bottom;
//    float h = size.height;
////     NSLog(@"offset: %f", offset.y);
////     NSLog(@"content.height: %f", size.height);
////     NSLog(@"bounds.height: %f", bounds.size.height);
////     NSLog(@"inset.top: %f", inset.top);
////     NSLog(@"inset.bottom: %f", inset.bottom);
////     NSLog(@"pos: %f of %f", y, h);
////    
////    float reload_distance = 10;
////    if(y > h + reload_distance) {
////        NSLog(@"load more rows");
////    }
////    else
////    {
////        NSLog(@"Refresh Control");
////    }
//    
//    if (offset.y < 0) {
//        isRefreshCalled = true;
//    }
//    else if (offset.y > 0)
//    {
//        isRefreshCalled = false;
//    }
//}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
//    if ([userListArray count]<totalCount && !isRefreshCalled) {
//
//            if (tblView.contentOffset.y >= (tblView.contentSize.height - tblView.bounds.size.height))
//            {
//                if (!isPageRefresing)
//                {
//                    isPageRefresing=YES;
//                    pageOffset= pageOffset+1;
//                    [self joinListAPI];
//                }
//                // we are at the end
//            }
//    }
    
}

#pragma mark Logout Button Click

- (IBAction)logoutBtnClk:(id)sender {
    [self logoutAPI];
}

-(void)logoutAPI
{
    if (SharedObj.isNetworkReachable) {
        MBProgressHUD * hud =  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.labelText = @"Loading...";
        
        NSMutableDictionary *Dict = [NSMutableDictionary new];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[[NSUserDefaults standardUserDefaults] objectForKey:@"OwnerDetails"] valueForKey:@"owner_id"]]forKey:@"id"];
        [Dict setValue:[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults]valueForKey:@"DeviceToken"]] forKey:@"device_token"];
        
        NSMutableDictionary *mainDict = [NSMutableDictionary new];
        [mainDict setObject:Dict forKey:@"owner"];
        
        [Common postServiceWithURL:[NSString stringWithFormat:@"%@/restaurants/logout",BASEURL] withParam:mainDict withCompletion:^(NSDictionary *response, BOOL success){
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (success)
            {
                NSLog(@"response in success = %@",response);
                
                [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"HasLogin"];
                
                if (SharedObj.isFromLogin) {
                    [self.navigationController popViewControllerAnimated:YES];
                }
                else
                {
                    [self performSegueWithIdentifier:@"gotoLogin" sender:nil];
                }

            }
            else
            {
                DisplayAlertControllerWithTitle(failureMessage, @"Q'd");
            }
        }];
    }
    else
    {
        DisplayAlertControllerWithTitle(noInternet, @"Q'd");
    }

}

#pragma mark UITextField methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == countryCodeTxtField)
    {
        countryCodeTblView.hidden = NO;
        [self.blurView bringSubviewToFront:countryCodeTblView];
        [noOfPeopleTxtField resignFirstResponder];
        [nameTxtField resignFirstResponder];
        [phnNoTxtField resignFirstResponder];
        
        isPageRefresing = YES;
        
        return NO;
    }
    else
    {
        return YES;
    }
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == phnNoTxtField || textField == noOfPeopleTxtField) {
//        [[IQKeyboardManager sharedManager] setEnable:false];
        countryCodeTblView.hidden = YES;

    }
    else if (textField == countryCodeTxtField)
    {
//        countryCodeTxtField = NO;
        
    }
    else{
        countryCodeTblView.hidden = YES;
//        [[IQKeyboardManager sharedManager] setEnable:true];
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == nameTxtField) {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    else if (textField == phnNoTxtField)
    {
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_Phone_NUMBERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
    else{
        NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_NUMBERS] invertedSet];
        NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
        return [string isEqualToString:filtered];
    }
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField == nameTxtField)
    {
        [phnNoTxtField becomeFirstResponder];
    }
    else if (textField == phnNoTxtField)
    {
        [noOfPeopleTxtField becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.blurView endEditing:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"gotoLogin"]) {
        ViewController *VC = (ViewController*)[segue destinationViewController];
    }
}


@end
