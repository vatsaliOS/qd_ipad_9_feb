//
//  MainVC.h
//  Qd
//
//  Created by SOTSYS028 on 16/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SWTableViewCell.h>
#import "FXBlurView.h"
#import "CKCircleView.h"


@interface MainVC : UIViewController<UITableViewDataSource,UITableViewDelegate,SWTableViewCellDelegate,UITextFieldDelegate,UIPopoverControllerDelegate,UIPopoverPresentationControllerDelegate>

@property CKCircleView* dialView;   // Third party

@property (weak, nonatomic) IBOutlet FXBlurView *blurView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *blurViewYConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *deleteViewCenterYconstraint;

@property (nonatomic, strong) UIPopoverController *userDataPopover;

@end
