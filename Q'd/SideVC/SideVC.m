//
//  SideVC.m
//  Qd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "SideVC.h"
#import "ViewController.h"

@interface SideVC ()

@end

@implementation SideVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}


#pragma mark TableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = (UITableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
//    cell.backgroundColor = [UIColor whiteColor];

    cell.textLabel.textColor = [UIColor darkGrayColor];
    //    cell.cellImgView.image = [UIImage imageNamed:@""];
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Notifications";
        cell.imageView.image = [UIImage imageNamed:@"Settings"];
    }
    else if (indexPath.row == 1)
    {
        cell.textLabel.text = @"Logout";
        cell.imageView.image = [UIImage imageNamed:@"Logout"];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"Hind-Regular" size:15.0f];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        [self performSegueWithIdentifier:@"SettingsVC" sender:nil];     // When Click on Notifications
    }
    else if (indexPath.row == 1){
        
        for (UIViewController *VC in self.navigationController.viewControllers) {
            if ([VC isKindOfClass:[ViewController class]]) {
                [self.navigationController popViewControllerAnimated:YES];
            }
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}


@end
