//
//  DrawerVC.m
//  Qd
//
//  Created by SOTSYS028 on 14/12/15.
//  Copyright © 2015 SOTSYS028. All rights reserved.
//

#import "DrawerVC.h"

@interface DrawerVC ()

@end

@implementation DrawerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIViewController *leftSideDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SideVC"];
    UIViewController *centerDrawerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainScreen"];
    
    self.centerViewController = centerDrawerViewController;
    
    self.leftDrawerViewController = leftSideDrawerViewController;
    self.maximumLeftDrawerWidth = 280;
    self.maximumRightDrawerWidth = 300;
    
    self.openDrawerGestureModeMask = MMOpenDrawerGestureModeNone;
    
    [self setShowsShadow:NO];
    [self setOpenDrawerGestureModeMask:MMOpenDrawerGestureModeAll];
    [self setCloseDrawerGestureModeMask:MMCloseDrawerGestureModeAll];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
