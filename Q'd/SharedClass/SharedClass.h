//
//  SharedClass.h
//  Qd
//
//  Created by SOTSYS028 on 12/01/16.
//  Copyright © 2016 SOTSYS028. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedClass : NSObject


@property (nonatomic,assign) BOOL hasLogin;
@property (nonatomic,assign) BOOL isFromLogin;

+(SharedClass *)objSharedClass;

-(BOOL)isNetworkReachable;

@end
